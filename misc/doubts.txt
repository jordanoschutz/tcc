Bolsa voluntária:
    - Só fazer funcionar o TCC
    - Trabalho do Ribas

Possibilidade de matrícula do TCC em 2019/2
    - É só 1 semestre
    - Não me matricular em 2019/1 e sim em 2019/2
    - Poderia terminar o TCC em 2019/1 e só me matricular em 2019/2
    - Seguir com esquema de 2 obrigatórias e 3 eletivas (c.c. muito pesadas, 3 eletivas dadas)
    - Ribas fala com o Cechin sobre quebra de pré-requisita para TCC em 2019/2
    - Professores: Geyer, Taisy (tranquilos), Carissimi (não tranquilo), Redes (preferir Marinho)

Créditos obrigatórios convertidos:
    - Deixar suspenso e aguardar Ribas falar com Cechin. Pode ser que eu não precise disso

Créditos complementares
    - 4 da bolsa volunatária com o Ribas
    - 4 do BEPiD (?)
    - Pode ser que seja necessário uma outra atividade extra (ver com Cechin) por ser 3 atividades extras:
        -> Atividades na semana acadêmica
        -> Ver com COMGRAD
        -> Lembras Ribas
