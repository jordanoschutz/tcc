#include "stdio.h"
#include "string.h"
#include "math.h"

// Função drawLine em console
void drawLine(int srcX, int srcY, int destX, int destY)
{
    // Linha vertical
    const char vert = '|';
    // Linha horizontal
    const char horiz = '-';
    // Linha com m > 0
    const char lineUp = '/';
    // Linha com m < 0
    const char lineDown = '\\';

    // Pontos devem ser ordenados por Y e depois por X antes de chegar aqui?
    int dx = destX - srcX;
    int dy = destY - srcY;
    char baseChar;

    // Se dx = dy = 0, então (srcX, srcY) = (destX, destY): nada a fazer
    if (!(dx == 0 && dy == 0))
    {
        // Deslocamento
        int m;

        if (dx == 0)
        {
            // Assíntota vertical
            baseChar = vert;
            // Só desloca em Y
            m = dy;
        }
        else if (dy == 0)
        {
            // Assíntota horizontal
            baseChar = horiz;
            // Só desloca em X
            m = dx;
        }
        else
        {
            // TODO deslocamento (m) para reta não assíntótica
            m = roundf((destX - srcX) / (destY - srcY));
            baseChar = m > 0 ? lineUp : lineDown;

            int i = srcX, j = srcY;
            for (; i != destX; i++, srcY += m)
            {
                printf("%c", baseChar); // Print at (i, j)
            }
        }
    }
}
