package tapatan

import core.*
import kotlin.math.abs

/**
 * Representa o tabuleiro do Tapatan restrito a dois jogadores
 */
class TapatanBoard(val player1: Player, val player2: Player) : Cloneable {

    /**
     * Tabuleiro do Tapatan 3x3 inicializado de forma fixa, com o meio livre
     * e as demais posições com peças intercaladas começando pelo "player1"
     */
    val boardMatrix = arrayOf(
        buildBoardRow(0, 3, player1),
        buildBoardRow(1, 3, player2),
        buildBoardRow(2, 3, player1)
    )

    /**
     * Executa uma jogada. Retorna PlayResult.UNKNOWN_PLAYER se o jogador não é "player1" ou "player2";
     * OCCUPIED_POSITION se a coordenada de destino já está ocupada; UNREACHABLE_POSITION se a coordenada
     * de destino é inválida (e.g., fora dos limites do tabuleiro); ou SUCCESS se a jogada foi bem-sucedida -
     * nesse caso o estado do tabuleiro é alterado se, e somente se, "simulate" for falso
     */
    fun play(player: Player, movement: PlayerMovement, simulate: Boolean = false) =
        if (!(player == player1 || player == player2)) {
            PlayResult.UNKNOWN_PLAYER
        } else if (isMovementValid(movement)) {
            val (fromI, fromJ) = movement.source
            val (toI, toJ) = movement.destiny

            val destinyPiece = boardMatrix[toI][toJ]

            if (destinyPiece.hasOwner()) {
                PlayResult.OCCUPIED_POSITION
            } else {
                if (!simulate) {
                    boardMatrix[fromI][fromJ].owner = null
                    boardMatrix[toI][toJ].owner = player
                }

                PlayResult.SUCCESS
            }
        } else {
            PlayResult.UNREACHABLE_POSITION
        }

    /**
     * Calcula todas as coordenadas às quais se pode chegar a partir de uma posição de origem
     */
    fun calculateReachingMovements(origin: Pair<Int, Int>): Iterable<Pair<Int, Int>> {
        val (i, j) = origin
        val result = mutableListOf<Pair<Int, Int>>()

        /* Permissão de se mover para a direita */
        val right = j + 1
        if (right < boardMatrix.size) {
            result.add(Pair(i, right))
        }

        /* Permissão de se mover para a esquerda */
        val left = j - 1
        if (left >= 0) {
            result.add(Pair(i, left))
        }

        /* Permissão de se mover para cima */
        val up = i - 1
        if (up >= 0) {
            result.add(Pair(up, j))
        }

        /* Permissão de se mover para baixo */
        val down = i + 1
        if (down < boardMatrix.size) {
            result.add(Pair(down, j))
        }

        /* Permissão de se mover na diagonal principal */
        if (i == j) {
            /* Descendo a diagonal principal */
            val goingDown = Pair(i + 1, j + 1)
            if (goingDown.first < boardMatrix.size) {
                result.add(goingDown)
            }

            /* Subindo a diagonal principal */
            val goingUp = Pair(i - 1, j - 1)
            if (goingUp.first >= 0) {
                result.add(goingUp)
            }
        }

        /* Permissão de se mover na diagonal secundária */
        if (i + j == boardMatrix.size - 1) {
            /* Descendo a diagonal secundária */
            val goingDown = Pair(i + 1, j - 1)
            if (goingDown.first < boardMatrix.size && goingDown.second >= 0) {
                result.add(goingDown)
            }

            /* Subindo a diagonal secundária */
            val goingUp = Pair(i - 1, j + 1)
            if (goingUp.first >= 0 && goingUp.second < boardMatrix.size) {
                result.add(goingUp)
            }
        }

        return result
    }

    /**
     * Determina se um movimento é válido. Um movimento é permitido se, e somente se,
     * as coordenadas de destino: estão dentro dos limites do tabuleiro; não apontam
     * para uma posição ocupada por algum jogador; o passo é unitário em linha ou coluna;
     * o passo é ou na diagonal principal ou na diagonal secundária
     */
    fun isMovementValid(movement: PlayerMovement): Boolean {
        val (srcI, srcJ) = movement.source
        val (dstI, dstJ) = movement.destiny

        /* Coordenadas de destino dentro dos limites do tabuleiro */
        return dstI in (0 until boardMatrix.size) && dstJ in (0 until boardMatrix.size)
            /* Posição de destino livre */
            && boardMatrix[dstI][dstJ].usable
            && (
            /* Movimento unitário em linha ou em coluna */
            abs((dstI - srcI) + (dstJ - srcJ)) == 1
                /* Movimentos na diagonal principal */
                || abs((srcI + srcJ) - (dstI + dstJ)) == 2
                /* Movimentos na diagonal secundária */
                || (srcI + srcJ == boardMatrix.size - 1 && dstI + dstJ == boardMatrix.size - 1)
            )
    }

    /**
     * Encontra um alinhamento no tabuleiro corrente. Se encontrado, o alinhamento é instanciado
     * e retornado; caso contrário, retorna-se nulo
     */
    fun searchAlignment(): BoardAlignment? {
        val rowAlignment = mutableListOf<Piece>()
        val mainDiagonal = mutableListOf<Piece>()
        val secondaryDiagonal = mutableListOf<Piece>()
        val columns = mutableMapOf<Int, MutableList<Piece>>()

        for (rows in boardMatrix) {
            for (piece in rows) {
                /* Acumulador de vetores linha */
                rowAlignment.add(piece)

                val (i, j) = piece.position

                /* Acumulador de vetores coluna */
                when (columns[j]) {
                    null -> columns[j] = mutableListOf(piece)
                    else -> columns[j]?.add(piece)
                }

                /* Construção de vetor da diagonal principal */
                when (i) {
                    j -> mainDiagonal.add(piece)
                }

                /* Construcão de vetor da diagonal secundária */
                when (i + j) {
                    boardMatrix.size - 1 -> secondaryDiagonal.add(piece)
                }
            }

            /* Teste de alinhamento horizontal */
            when {
                isAligned(rowAlignment) -> {
                    /* Alinhamento horizontal encontrado */
                    return BoardAlignment(rowAlignment.toTypedArray(), AlignmentType.HORIZONTAL)
                }
            }

            rowAlignment.clear()
        }

        return when {
            /* Teste de alinhamento em diagonal principal */
            isAligned(mainDiagonal) -> BoardAlignment(
                mainDiagonal.toTypedArray(),
                AlignmentType.MAIN_DIAGONAL
            )

            /* Teste de alinhamento em diagonal secundária */
            isAligned(secondaryDiagonal) -> BoardAlignment(
                secondaryDiagonal.toTypedArray(),
                AlignmentType.SECONDARY_DIAGONAL
            )

            else -> {
                /* Teste de alinhamento vertical */
                val columnAlignment =
                    columns
                        .map { kv -> kv.value }
                        .firstOrNull { vector -> isAligned(vector) }

                when (columnAlignment) {
                    null -> null
                    else -> BoardAlignment(columnAlignment.toTypedArray(), AlignmentType.VERTICAL)
                }
            }
        }
    }

    /**
     * Cópia profunda da instância
     */
    public override fun clone(): TapatanBoard {
        val result = TapatanBoard(player1, player2)

        for (i in 0 until boardMatrix.size) {
            for (j in 0 until boardMatrix.size) {
                result.boardMatrix[i][j] = boardMatrix[i][j].clone()
            }
        }

        return result
    }

    /**
     * Determina se existe um alinhamento de peças em um vetor. Há um alinhamento se, e somente se,
     * todas as peças têm como dono o mesmo jogador e este não é nulo
     */
    private fun isAligned(vector: Iterable<Piece>) =
        vector.all { it.hasOwner() } && vector.distinctBy { it.owner?.id }.size == 1

    /**
     * Gera uma linha do tabuleiro. Essa linha vai ser gerada com posições intercaladas
     * iniciando com o parâmetro "firstPlayer". Por exemplo: se "firstPlayer" for o Jogador 1,
     * então a linha construída corresponderá a [Jogador 1, VAZIO, Jogador 2, VAZIO, ...]
     */
    private fun buildBoardRow(rowIndex: Int, size: Int, firstPlayer: Player): Array<Piece> {
        val row = mutableListOf<Piece>()
        val possiblePlayers = arrayOf(
            firstPlayer,
            null,
            when (firstPlayer) {
                player1 -> player2
                else -> player1
            }
        )

        var cursorPlayer = 0
        val nextPlayer = {
            when (cursorPlayer) {
                possiblePlayers.size -> cursorPlayer = 0
            }

            possiblePlayers[cursorPlayer++]
        }

        for (column in 0 until size) {
            val newPiece = Piece(Pair(rowIndex, column))
            newPiece.owner = nextPlayer()

            row.add(newPiece)
        }


        return row.toTypedArray()
    }
}
