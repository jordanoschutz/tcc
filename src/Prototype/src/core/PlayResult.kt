package core

/**
 * Enumera os possíveis resultados de uma jogada
 */
enum class PlayResult {
    /**
     * Jogada aceita
     */
    SUCCESS,
    /**
     * Posição inatingível, i.e., fora dos limites do tabuleiro
     * ou incoerente com a especificação do jogo
     */
    UNREACHABLE_POSITION,
    /**
     * Posição ocupada por outro jogador
     */
    OCCUPIED_POSITION,
    /**
     * Jogador desconhecido
     */
    UNKNOWN_PLAYER,
}
