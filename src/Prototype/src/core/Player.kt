package core

/**
 * Representa um jogador
 */
data class Player(
    /**
     * Identificador do jogador (e.g.: Jogador 1)
     */
    val id: Int,
    /**
     * Nome do jogador
     */
    var name: String,
    /**
     * Tipo do jogador, i.e., MIN ou MAX
     */
    val type: PlayerType,
    /**
     * Número de vitórias acumuladas pelo jogador
     */
    var victories: Int = 0,
    /**
     * Número de derrotas acumuladas pelo jogador
     */
    var losses: Int = 0,
    /**
     * Número de empates acumulados pelo jogador
     */
    var draws: Int = 0
) {
    /**
     * Determina se um player é igual a uma instância de um objeto. Retorna verdadeiro
     * se, e somente se, "other" é um Player e sua propriedade "id" é igual a da instância
     */
    override fun equals(other: Any?) = other is Player && other.id == id

    /**
     * Hash code uma instância de player (padrão IntelliJ)
     */
    override fun hashCode(): Int {
        var result = id

        result = 31 * result + name.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + victories
        result = 31 * result + losses
        result = 31 * result + draws

        return result
    }
}
