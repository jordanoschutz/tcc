package core

/**
 * Enumera os tipos de alinhamento possíveis
 */
enum class AlignmentType {
    /**
     * Alinhamento em linha
     */
    HORIZONTAL,
    /**
     * Alinhamento em coluna
     */
    VERTICAL,
    /**
     * Alinhamento na diagonal principal
     */
    MAIN_DIAGONAL,
    /**
     * Alinhamento na diagonal secundária
     */
    SECONDARY_DIAGONAL
}
