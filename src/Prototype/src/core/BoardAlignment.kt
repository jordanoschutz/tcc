package core

/**
 * Representa um alinhamento no tabuleiro
 */
class BoardAlignment (
    /**
     * As peças alinhadas
     */
    val linedUpPieces: Array<Piece>,
    /**
     * O tipo do alinhamento
     */
    val type: AlignmentType
) {

    /**
     * O jogador que alinhou as peças
     */
    val player = linedUpPieces.first().owner!!
}
