package core

/**
 * Representa uma jogada
 */
data class PlayerMovement(
    /**
     * A posição de origem da peça
     */
    val source: Pair<Int, Int>,
    /**
     * A posição de destino da peça
     */
    val destiny: Pair<Int, Int>
) {
    /**
     * Determina se a instância é igual a outro objeto. Estes são iguais se, e somente se, "other" é um PlayerMovement
     * e se as coordenadas de origem e destino são rigorosamente iguais
     */
    override fun equals(other: Any?) = other is PlayerMovement && other.source == source && other.destiny == destiny

    /**
     * Hash code uma instância de player (padrão IntelliJ)
     */
    override fun hashCode(): Int {
        var result = source.hashCode()
        result = 31 * result + destiny.hashCode()
        return result
    }
}
