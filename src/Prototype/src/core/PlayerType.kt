package core

/**
 * Enumera os diferentes tipos de jogador de acordo
 * com o algoritmo Minimax
 */
enum class PlayerType {
    /**
     * Jogador que busca minimizar o ganho
     */
    MIN,
    /**
     * Jogador que busca maximizar o ganho
     */
    MAX
}
