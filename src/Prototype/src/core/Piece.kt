package core

/**
 * Representa uma peça de um jogo de tabuleiro inicializada
 * com uma posição (i, j) de um tabuleiro
 */
class Piece(
    /**
     * Posição (i, j) ocupada pela peça
     */
    val position: Pair<Int, Int>
) : Cloneable {
    /**
     * O jogador dono da peça. Se nulo, então nenhum
     * jogador está ocupando a peça
     */
    var owner: Player? = null
        set(value) {
            field = value

            usable = field == null
        }


    /**
     * Indica se a peça é utilizável, isto é, pode receber um jogador dono
     * Se falso, então a peça ocupa uma posição fixa no tabuleiro ou existe
     * um jogador que a detém
     */
    var usable: Boolean = true
        private set

    /**
     * Indica se a peça possui um jogador. Retorna verdadeiro se,
     * e somente se, há uma referência a algum jogador
     */
    fun hasOwner() = owner != null

    /**
     * Cria uma cópia profunda dessa instância de Piece
     */
    public override fun clone(): Piece {
        val result = Piece(Pair(position.first, position.second))
        result.owner = owner

        return result
    }
}
