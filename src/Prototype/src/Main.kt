import ai.minimax.Minimax
import ai.minimax.TreeNode
import board.spec.BoardSpecReader
import core.Player
import core.PlayerType
import game.controllers.TapatanController
import game.graphics.GameDrawer
import tapatan.TapatanBoard
import java.io.File
import java.util.*

/**
 * Entry point do protótipo
 */
fun main() {
    println(">>> Main do TCC")

    val operations = Operation.supportedOperations()
    var selectedMainOperation: Operation?

    do {
        println()

        println(">>> Modos de operação")

        operations.forEach { println(">>>>>> ${it.id} - ${it.displayName}") }

        print(">>> Escolha um modo: ")
        selectedMainOperation = try {
            val response = readLine()?.toInt()
            operations.firstOrNull { it.id == response }
        } catch (_: Throwable) {
            println(">>> Opção inválida!")
            null
        }
    } while (selectedMainOperation == null)

    println()

    println(">>> Certo! Você operará no ${selectedMainOperation.displayName}")

    println()

    var loop: Boolean

    do {
        var suboperation: Operation.Suboperation?

        do {
            println()

            println(">>> Opções do ${selectedMainOperation.displayName}")

            selectedMainOperation.suboperations.forEach {
                println(">>>>>> ${it.id} - ${it.displayName}")
            }

            print(">>> Escolha uma operação: ")

            suboperation = try {
                val response = readLine()?.toInt()
                selectedMainOperation.suboperations.firstOrNull { it.id == response }
            } catch (_: Throwable) {
                println(">>> Opção inválida!")
                null
            }
        } while (suboperation == null)

        println()

        println(">>> Aperte Enter para executar a operação ${suboperation.displayName}")

        readLine()

        suboperation.action()

        println()

        print(">>> Deseja continuar? [S/s para sim]: ")
        loop = readLine()?.toLowerCase()?.equals("s", true) == true
    } while (loop)
}

/**
 * Imprime uma árvore Minimax no estilo do comando DIR do MS-DOS
 */
@Suppress("UNUSED_PARAMETER")
private fun printTree(depth: Long = 4.toLong(), saveInFile: Boolean = false): Nothing = TODO("Print da árvore Minimax não implementado")

/**
 * Imprime os valores dos nodos da árvore Minimax em linhas. Cada linha representa um nível. Dessa forma, é possível verificar a distribuição
 * de valores de acordo com a profundidade do nodo
 */
private fun printValuesInLevels() {
    val (p1, p2) = Pair(Player(1, "MAX", PlayerType.MAX), Player(2, "MIN", PlayerType.MIN))
    val minimax = Minimax(p1, p2)
    val root = minimax.buildTree(
        TapatanBoard(p1, p2),
        8
    )
    val queue = LinkedList<TreeNode>(listOf(root))
    val levelsOutput = mutableMapOf<Long, MutableList<Double>>()

    while (queue.isNotEmpty()) {
        val current = queue.remove()

        if (levelsOutput.containsKey(current.level)) {
            levelsOutput[current.level]?.add(current.value)
        } else {
            levelsOutput[current.level] = mutableListOf(current.value)
        }

        if (current.children.isNotEmpty()) {
            queue.addAll(current.children)
        }
    }

    println(">>> Valores por nível")
    println()

    levelsOutput.forEach { level, values ->
        println("Nível $level: ${values.distinct().joinToString()}")
    }

    println()
    println(">>>")
}

/**
 * Imprime no console o tabuleiro do Tapatan
 */
private fun printTapatanBoard() = GameDrawer.drawBoard(
    TapatanBoard(
        Player(1, "MAX", PlayerType.MAX),
        Player(2, "MIN", PlayerType.MIN)
    )
)

/**
 * Executa a leitura de um arquivo de especificação BOARD
 */
private fun loadBoardFromSpec() {
    val file = File("assets/boards/tapatan.board")
    println(">>> Carregando arquivo BOARD ${file.absolutePath}...")

    try {
        val materializedBoard = BoardSpecReader.read(file)
        println(">>> Tabuleiro materializado com sucesso: $materializedBoard")
    } catch (e: Throwable) {
        println(">>> Exceção ${e.javaClass.name}: ${e.message}")
    }
}

private fun printTapatanBoardFromSpec() {
    val file = File("assets/boards/tapatan.board")
    val materializedBoard = BoardSpecReader.read(file)

    GameDrawer.drawMaterializedBoard(materializedBoard)
}

/**
 * Representa as operações suportadas por esta Main, inicializa por um identificador, um nome apresentável para o usuário
 * e suboperações associadas a cada operação
 */
private class Operation(val id: Int, val displayName: String, val suboperations: Array<Suboperation>) {

    companion object {
        /**
         * Recupera as operações e suboperações suportadas por esta Main
         */
        fun supportedOperations(): Array<Operation> =
            arrayOf(
                Operation(
                    1,
                    "Modo Debug",
                    arrayOf(
                        Suboperation(1, "Imprimir tabuleiro na tela") { printTapatanBoard() },
                        Suboperation(2, "Imprimir árvore do Minimax") { printTree() },
                        Suboperation(3, "Imprimir valores dos nodos em níveis") { printValuesInLevels() },
                        Suboperation(4, "Materializar tabuleiro a partir de arquivo BOARD") { loadBoardFromSpec() },
                        Suboperation(5, "Imprimir Tapatan por arquivo BOARD") { printTapatanBoardFromSpec() }
                    )
                ),
                Operation(
                    2,
                    "Modo Release",
                    arrayOf(
                        Suboperation(1, "Controller do Tapatan CPU vs Usuário") { TapatanController().startGame() }
                    )
                )
            )
    }

    /**
     * Um suboperação, identificada por um número e rotulada apropriadamente, capaz de disparar uma determinada ação
     */
    data class Suboperation(
        val id: Int,
        val displayName: String,
        val action: () -> Unit
    )
}
