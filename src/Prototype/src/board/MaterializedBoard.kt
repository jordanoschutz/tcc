package board

import board.spec.definition.Edge
import board.spec.definition.Vertex

class MaterializedBoard(
    val height: Int,
    val width: Int,
    val vertices: Iterable<Vertex>,
    val edges: Iterable<Edge>
) {
    var boardMatrix = buildBoardMatrix()
        private set

    fun cloneExpandedCopy(dX: Int, dY: Int): MaterializedBoard {
        val newHeight = height + dY * height.dec()
        val newWidth = width + dX * width.dec()

        val newVertices = vertices.map { vertex ->
            Vertex(
                vertex.identifier,
                vertex.usable,
                if (vertex.row > 0) vertex.row * dY + vertex.row else vertex.row,
                if (vertex.column > 0) vertex.column * dX + vertex.column else vertex.column
            )
        }

        val newEdges = edges.map { edge ->
            Edge(
                edge.identifier,
                edge.usable,
                Pair(
                    newVertices.first { v -> v == edge.vertices.first },
                    newVertices.first { v -> v == edge.vertices.second }
                )
            )
        }

        return MaterializedBoard(
            newHeight,
            newWidth,
            newVertices,
            newEdges
        )
    }

    private fun buildBoardMatrix(): Array<Array<Element>> =
        Array(height) { row ->
            Array(width) { column ->
                val vertex = vertices.firstOrNull { v ->
                    v.row == row && v.column == column
                }

                val (v, e, concrete) = when (vertex) {
                    null -> {
                        Triple(
                            Vertex(-(row + column), false, row, column),
                            emptyList(),
                            false
                        )
                    }
                    else -> {
                        val neighbors = edges.mapNotNull { e ->
                            when (vertex) {
                                e.vertices.first -> e.vertices.second
                                e.vertices.second -> e.vertices.first
                                else -> null
                            }
                        }

                        Triple(vertex, neighbors, true)
                    }
                }

                Element(v, e, concrete)
            }
        }

    data class Element(
        val vertex: Vertex,
        val neighbors: Iterable<Vertex>,
        val concrete: Boolean = true
    )
}
