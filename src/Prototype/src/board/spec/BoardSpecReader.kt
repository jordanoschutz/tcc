package board.spec

import board.MaterializedBoard
import board.spec.definition.Edge
import board.spec.definition.Vertex
import board.spec.definition.exceptions.*
import java.io.File
import java.io.FileNotFoundException

/**
 * Responsável por ler um arquivo de especificação BOARD, capaz de descrever
 * um tabuleiro bidimensional com vértices e arestas de tamanho arbitrário
 */
object BoardSpecReader {

    /**
     * Constante booleana da especificação BOARD que indica valor verdadeiro
     */
    private const val SPEC_BOOL_TRUE = "1"
    /**
     * Constante booleano da especificação BOARD que indica valor falso
     */
    private const val SPEC_BOOL_FALSE = "0"

    /**
     * Expressão regular para o padrão de declaração de espaço bidimensional na especificação BOARD
     */
    private val spaceRegex = "s\\s+(\\d+)\\s+(\\d+)".toRegex(RegexOption.IGNORE_CASE)
    /**
     * Expressão regular para o padrão de declaração de um vértice na especificação BOARD
     */
    private val vertexRegex = "v(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+([01])".toRegex(RegexOption.IGNORE_CASE)
    /**
     * Expressão regular para o padrão de declaração de uma aresta na especificação BOARD
     */
    private val edgeRegex = "e(\\d+)\\s+v(\\d+)\\s+v(\\d+)\\s+([01])".toRegex(RegexOption.IGNORE_CASE)

    /**
     * Parse de um arquivo, por nome, para materialização de um tabuleiro na especificação BOARD
     */
    fun read(filename: String) = read(File(filename))

    /**
     * Parse de um arquivo, por referência, para materialização de um tabuleiro na especificação BOARD
     */
    fun read(file: File): MaterializedBoard =
        when {
            file.exists() -> {
                when {
                    file.isFile -> {
                        val contentsOfFile = file.readText()
                        val spaceMatches = spaceRegex.findAll(contentsOfFile)
                        val spaceCount = spaceMatches.count()

                        when (spaceCount) {
                            0 -> throw SpaceNotFoundException("Nenhum espaço 2D definido para o arquivo")

                            1 -> {
                                val valuesForSpace = spaceMatches.first().groupValues
                                val (heightSpace, widthSpace) = Pair(
                                    valuesForSpace[1].toInt(),
                                    valuesForSpace[1].toInt()
                                )

                                when {
                                    isSpaceSizeValid(heightSpace, widthSpace) -> {
                                        val vertexMatches = vertexRegex.findAll(contentsOfFile)
                                        val vertices = mutableListOf<Vertex>()

                                        for (vm in vertexMatches) {
                                            val groupValues = vm.groupValues
                                            val vertex = Vertex(
                                                groupValues[1].toInt(),
                                                groupValues[4] == SPEC_BOOL_TRUE,
                                                groupValues[2].toInt(),
                                                groupValues[3].toInt()
                                            )

                                            if (isVertexValid(vertex, widthSpace, heightSpace)) {
                                                vertices.add(vertex)
                                            } else {
                                                throw InvalidVertexException(
                                                    "Vértice com posição inválida: " +
                                                        "ID = ${vertex.identifier}, Posição (X, Y) = (${vertex.column}, ${vertex.row})"
                                                )
                                            }
                                        }

                                        when {
                                            vertices.any() -> {
                                                val edgeMatches = edgeRegex.findAll(contentsOfFile)
                                                val edges = mutableListOf<Edge>()

                                                for (em in edgeMatches) {
                                                    val groupValues = em.groupValues
                                                    val (v1, v2) = Pair(groupValues[2].toInt(), groupValues[3].toInt())
                                                    val (firstVertex, secondVertex) = Pair(
                                                        vertices.firstOrNull { it.identifier == v1 },
                                                        vertices.firstOrNull { it.identifier == v2 }
                                                    )

                                                    if (firstVertex == null || secondVertex == null) {
                                                        throw InvalidVertexException(
                                                            "Par de vértices (V$v1, V$v2) contém ao menos um vértice não declarado"
                                                        )
                                                    } else {
                                                        edges.add(
                                                            Edge(
                                                                groupValues[1].toInt(),
                                                                groupValues[4] != SPEC_BOOL_FALSE,
                                                                Pair(firstVertex, secondVertex)
                                                            )
                                                        )
                                                    }
                                                }

                                                when {
                                                    edges.any() -> MaterializedBoard(
                                                        heightSpace,
                                                        widthSpace,
                                                        vertices,
                                                        edges
                                                    )

                                                    else -> throw NoEdgesFoundException("Nenhuma aresta especificada no arquivo")
                                                }
                                            }

                                            else -> throw NoVerticesSpecifiedException("Nenhum vértice especificado no arquivo")
                                        }
                                    }

                                    else -> throw UnsupportedSpaceSizeException(
                                        "Espaço 2D deve ter largura e altura maiores que zero. Altura/largura informados: $heightSpace/$widthSpace"
                                    )
                                }
                            }

                            else -> throw TooManySpacesException("Apenas 1 espaço 2D pode ser definido. Encontrados: $spaceCount")
                        }
                    }

                    else -> throw IllegalArgumentException("Arquivo ${file.absolutePath} é um diretório")
                }
            }

            else -> throw FileNotFoundException("Arquivo ${file.absolutePath} não encontrado")
        }

    /**
     * Determina se a definição de tamanho de um espaço bidimensional é válido. Um espaço é válido se, e somente se,
     * tanto a largura quanto a altura são maiores que zero
     */
    private fun isSpaceSizeValid(height: Int, width: Int) = height > 0 && width > 0

    /**
     * Determina se uma declaração de vértice é válida. Um vértice é válido se, e somente se, suas posições (x, y) está
     * delimitada pelo tamanho do espaço bidimensional no qual ele foi declarado, nos intervalos [0, largura_espaco_2D) para X
     * e [0, altura_espaco_2D) para Y
     */
    private fun isVertexValid(v: Vertex, maxX: Int, maxY: Int) = v.row in 0 until maxY && v.column in 0 until maxX
}
