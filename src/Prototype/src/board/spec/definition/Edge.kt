package board.spec.definition

/**
 * Representa uma aresta na especificação do arquivo BOARD
 */
data class Edge(
    /**
     * Identificador da aresta
     */
    override val identifier: Int,
    /**
     * Indica se a aresta pode ser visitada pelo jogador
     */
    override val usable: Boolean,
    /**
     * Vértices sobre os quais a aresta incide
     */
    val vertices: Pair<Vertex, Vertex>
) : Specificable {
    /**
     * Determina se uma instância é igual a própria instância de Edge. Ambas são iguais se, e somente se,
     * "other" é uma instância de Edge e os identificadores são iguais
     */
    override fun equals(other: Any?): Boolean = other is Edge && other.identifier == identifier

    /**
     * Código hash da instância conforme implementação IntelliJ
     */
    override fun hashCode(): Int {
        var result = identifier
        result = 31 * result + usable.hashCode()
        result = 31 * result + vertices.hashCode()
        return result
    }
}
