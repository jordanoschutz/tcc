package board.spec.definition

/**
 * Representa um vértice de acordo com a especificação BOARD
 */
data class Vertex(
    /**
     * Identificador do vértice
     */
    override val identifier: Int,
    /**
     * Indica se o vértice é utilizável pelo jogador
     */
    override val usable: Boolean,
    /**
     * Linha a qual o vértice pertence
     */
    val row: Int,
    /**
     * Coluna a qual o vértice pertence
     */
    val column: Int
) : Specificable {
    /**
     * Determina se um vértice é igual a uma outro instância de objeto. "other" é igual a própria instância se,
     * e somente se, "other" é uma instância de Vertex e os identificadores são iguais
     */
    override fun equals(other: Any?): Boolean = other is Vertex && other.identifier == identifier

    /**
     * Código hash de uma instância de Vertex conforme implementação IntelliJ
     */
    override fun hashCode(): Int {
        var result = identifier
        result = 31 * result + usable.hashCode()
        result = 31 * result + row
        result = 31 * result + column
        return result
    }
}
