package board.spec.definition

/**
 * Define um protocolo para um objeto especificável de acordo com o arquivo BOARD
 */
interface Specificable {
    /**
     * Identificador do objeto
     */
    val identifier: Int
    /**
     * Indicador de usabilidade do objeto
     */
    val usable: Boolean
}
