package board.spec.definition.exceptions

/**
 * Exceção levantada quando um arquivo BOARD não especifica espaço
 */
class SpaceNotFoundException : Throwable {
    constructor() : super()

    constructor(
        message: String?,
        cause: Throwable?
    ) : super(message, cause)

    constructor(
        message: String?
    ) : super(message)

    constructor(
        cause: Throwable?
    ) : super(cause)

    constructor(
        message: String?,
        cause: Throwable?,
        enableSuppression: Boolean,
        writableStackTrace: Boolean
    ) : super(message, cause, enableSuppression, writableStackTrace)
}
