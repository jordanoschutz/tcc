package board.spec.definition.exceptions

/**
 * Exceção levantada quando um espaço bidimensional é declarado com dimensões inválidas
 * na especificação do arquivo BOARD
 */
class UnsupportedSpaceSizeException : Throwable {
    constructor() : super()

    constructor(
        message: String?,
        cause: Throwable?
    ) : super(message, cause)

    constructor(
        message: String?
    ) : super(message)

    constructor(
        cause: Throwable?
    ) : super(cause)

    constructor(
        message: String?,
        cause: Throwable?,
        enableSuppression: Boolean,
        writableStackTrace: Boolean
    ) : super(message, cause, enableSuppression, writableStackTrace)
}
