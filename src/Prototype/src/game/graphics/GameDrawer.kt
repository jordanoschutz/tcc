package game.graphics

import board.MaterializedBoard
import board.spec.definition.Edge
import core.Player
import tapatan.TapatanBoard
import java.util.*
import kotlin.math.round

/**
 * Singleton para gráficos do protótipo em console
 */
object GameDrawer {

    /**
     * Símbolo de espaço vazio no tabuleiro
     */
    const val EMPTY_SYMBOL = "?"

    private const val CHR_VERTICAL = "|"
    private const val CHR_HORIZONTAL = "-"
    private const val CHR_LINE_UP = "/"
    private const val CHR_LINE_DOWN = "\\"

    /**
     * Cria uma representação em String de um tabuleiro do Tapatan
     */
    fun boardAsString(board: TapatanBoard): String {
        val matrix = board.boardMatrix
        val stop = matrix.size - 1
        val buffer = StringBuilder()
        val space = " "
        val diagonalQueue = LinkedList(listOf("\\", "/", space, "/", "\\", space))
        val nextDiagonalString = {
            val next = diagonalQueue.remove()
            diagonalQueue.add(next)
            next
        }

        for (i in 0..stop) {
            for (j in 0..stop) {
                val symbol = playerToSymbol(matrix[i][j].owner)

                buffer.append("$symbol ")

                if (j < stop) {
                    buffer.append("-".repeat(4))
                }
            }

            if (i < stop) {
                buffer.append("\n")

                repeat(matrix.size) {
                    buffer
                        .append("|")
                        .append(space.repeat(2))
                        .append(nextDiagonalString())
                        .append(space.repeat(2))
                }

                buffer.append("\n")
            }
        }

        return buffer.toString()
    }

    /**
     * Desenha um tabuleiro do Tapatan em console, utilizando símbolos diferentes
     * para jogador e espaços vazios
     */
    fun drawBoard(board: TapatanBoard) = println(boardAsString(board))

    /**
     * Mapeia um jogador ou um espaço vazio para um símbolo gráfico
     */
    fun playerToSymbol(player: Player?) = player?.id?.toString() ?: EMPTY_SYMBOL

    fun drawMaterializedBoard(board: MaterializedBoard, shouldExpand: Boolean = true) {
        val (expansionFactorX, expansionFactorY) = Pair(2, 2)
        val drawable = if (shouldExpand) board.cloneExpandedCopy(expansionFactorX, expansionFactorY) else board
        val viewMatrix = Array(drawable.height) { row ->
            Array(drawable.width) { column ->
                if (drawable.boardMatrix[row][column].concrete) {
                    drawable.boardMatrix[row][column].vertex.identifier.toString()
                } else {
                    ""
                }
            }
        }

        val visitedEdges = mutableListOf<Edge>()

        drawable.edges.forEach { edge ->
            when (edge) {
                !in visitedEdges -> {
                    val (v1, v2) = edge.vertices

                    drawLineOnViewMatrix(
                        viewMatrix,
                        v1.column,
                        v1.row,
                        v2.column,
                        v2.row
                    )
                }
            }
        }

        viewMatrix.forEach { row ->
            println(row.joinToString(separator = ""))
        }
    }

    private fun drawLineOnViewMatrix(viewMatrix: Array<Array<String>>, srcX: Int, srcY: Int, dstX: Int, dstY: Int) {
        val dx = dstX - srcX
        val dy = dstY - srcY

        if (!(dx == 0 && dy == 0)) {
            var stepX: Int
            var stepY: Int

            val (stringToDraw, loop, update) = when {
                dx == 0 -> {
                    stepX = srcX
                    stepY = if (dy > 0) srcY.inc() else srcY.dec()

                    Triple(
                        CHR_VERTICAL,
                        {
                            stepY != dstY
                        },
                        {
                            if (dy > 0) stepY++ else stepY--
                        }
                    )
                }

                dy == 0 -> {
                    stepY = srcY
                    stepX = if (dx > 0) srcX.inc() else srcX.dec()

                    Triple(
                        CHR_HORIZONTAL,
                        {
                            stepX != dstX
                        },
                        {
                            if (dx > 0) stepX++ else stepX--
                        }
                    )
                }

                else -> {
                    stepX = if (dx > 0) srcX.inc() else srcX.dec()
                    stepY = if (dy > 0) srcY.inc() else srcY.dec()

                    val m = round(dx.toDouble() / dy.toDouble()).toInt()

                    Triple(
                        if (m > 0) CHR_LINE_UP else CHR_LINE_DOWN,
                        {
                            !(stepX == dstX || stepY == dstY)
                        },
                        {
                            if (dx > 0) stepX++ else stepX--
                            stepY += m
                        }
                    )
                }
            }

            while (loop()) {
                viewMatrix[stepY][stepX] = stringToDraw
                update()
            }
        }
    }
}
