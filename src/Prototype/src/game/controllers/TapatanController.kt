package game.controllers

import ai.minimax.Minimax
import core.*
import game.graphics.GameDrawer
import tapatan.TapatanBoard

class TapatanController {

    /**
     * Jogador MAX: sempre a CPU
     */
    private val _maxPlayer = Player(1, "CPU", PlayerType.MAX)
    /**
     * Jogador MIN: o usuário que interage pelo console
     */
    private val _minPlayer = Player(2, "Usuário", PlayerType.MIN)
    /**
     * Default: CPU inicia o jogo
     */
    private var _cpuBeginsGame = true
    /**
     * Expressão regular para validação da jogada do usuário
     */
    private val _regexPlayerMovement = "([abc])[\\s\\S]*[\\s\\S]*([1-3])[\\s\\S]*,[\\s\\S]*([abc])[\\s\\S]*[\\s\\S]*([1-3])"
        .toRegex(RegexOption.IGNORE_CASE)

    /**
     * Inicia um novo jogo do Tapatan
     */
    fun startGame() {
        /* Imprime o título da aplicação */
        printTitle()

        println()

        /* Verifica o nome do jogador e se este deseja iniciar o jogo, qual o nível de dificuldade, etc. */
        askPreferences()

        println()

        /* Breve explicação sobre o modo de jogar */
        explain()

        println()
        readLine()

        tapatanLoop()
    }

    /**
     * Imprime o título do jogo
     */
    private fun printTitle() {
        println("*".repeat(24))
        println("${"=".repeat(7)} TAPATAN  ${"=".repeat(7)}")
        println("*".repeat(24))
    }

    /**
     * Verifica as preferências do usuário: nome e se este deseja iniciar a partida
     */
    private fun askPreferences() {
        print(">>> Qual seu nome? ")
        _minPlayer.name = readLine() ?: _minPlayer.name
        println(">>> Bem-vindo, ${_minPlayer.name}!")

        println()

        print(">>> Você deseja iniciar o jogo? Digite [S/s] para sim: ")
        _cpuBeginsGame = !(readLine() ?: "").equals("s", true)
        println(">>> Certo! Você${if (_cpuBeginsGame) " não" else ""} vai iniciar o jogo.")
    }

    /**
     * Imprime no console uma breve explicação sobre como funciona o Tapatan e como se deve fazer as jogadas
     */
    private fun explain() {
        println(">>> ${_minPlayer.name}, suas peças serão representadas por \"${GameDrawer.playerToSymbol(_maxPlayer)}\".")
        println(">>> As peças da CPU, seu adversário, serão representadas por \"${GameDrawer.playerToSymbol(_minPlayer)}\".")
        println(">>> Posições marcadas com \"${GameDrawer.EMPTY_SYMBOL}\" representam espaços vazios.")
        println(">>> Lembre-se: você pode deslocar as peças em um passo apenas, em linha, coluna ou diagonais.")
        println(">>> Para jogar você deverá especificar as coordenadas de linha e coluna de origem")
        println(">>> separadas por \",\" das coordenadas de linha e coluna de destino. Use [A/a] para a primeira linha, [B/b] para a segunda linha ")
        println(">>> e [C/c] para a terceira linha; use 1, 2 e 3 para as colunas. Complicado? Por exemplo:")
        println(">>> \"A1,B1\" quer dizer que você vai mover a peça que está na primeira linha e na primeira coluna para")
        println(">>> a posição livre que está na segunda linha e na primeira coluna.")
        println(">>> Ganha o jogo aquele que formar um alinhamento vertical, horizontal ou em diagonal (estilo Jogo da Velha).")
        println(">>> Pressione \"Enter\" para iniciar...")
    }

    /**
     * Loop do jogo: enquanto o usuário quiser prosseguir com o jogo, permanece nesse método. O controle de fim de jogo
     * é feito dentro do próprio laço, o qual também percorre a árvore Minimax e executa os chaveamentos de jogadas
     * e substituição de nodo raiz conforme o rumo do jogo
     */
    private fun tapatanLoop() {
        var keepGoing = true
        var playCount = 0

        val (startPlayer, otherPlayer) = if (_cpuBeginsGame) Pair(_maxPlayer, _minPlayer) else Pair(_minPlayer, _maxPlayer)
        val minimax = Minimax(startPlayer, otherPlayer)
        var treeRoot = minimax.buildTree(
            TapatanBoard(_maxPlayer, _minPlayer),
            MINIMAX_DEPTH_TREE
        )

        val printBoard = {
            println()
            GameDrawer.drawBoard(treeRoot.board)
            println()
        }

        while (keepGoing) {
            when (playCount) {
                0 -> {
                    printBoard()
                    print(">>> Pressione enter para iniciar o jogo...")
                    readLine()
                }
            }

            if (treeRoot.isLeaf()) {
                println(">>> CPU calculando próximas jogadas...")
                minimax.demandMoreTreeLevels(treeRoot, MINIMAX_DEPTH_TREE)
            }

            println("${"=".repeat(7)} Jogada ${++playCount} ${"=".repeat(7)}")
            println(">>> Jogador: ${treeRoot.player.name}")

            when (treeRoot.player.type) {
                PlayerType.MAX -> {
                    println(">>> CPU preparando jogada...")

                    val nextNode = minimax.selectMostValuableNode(treeRoot)

                    when (nextNode) {
                        null -> {
                            println(">>> Aparentemente, a CPU não sabe mais o que fazer :(")
                            println(">>> Pressione enter para encerrar...")
                            readLine()
                            System.exit(1)
                        }

                        else -> {
                            treeRoot = nextNode
                        }
                    }
                }

                PlayerType.MIN -> {
                    var shouldRequestMovement = true
                    var movement: PlayerMovement?

                    while (shouldRequestMovement) {
                        print(">>> Digite sua jogada: ")
                        movement = playerMovementFromInputString(readLine() ?: "")

                        if (movement == null) {
                            println(">>> Jogada inválida! Use [A/a], [B/b], [C/c] para linha e 1..3 para coluna. Exemplo: \"A1,B1\"")
                            println(">>> Pressione enter para tentar novamente...")
                            readLine()
                        } else {
                            /*
                                Usabilidade ganhando da funcionalidade: bastaria percorrer os nodos filhos de treeRoot, no entanto,
                                o retorno não seria tão claro quanto os fornecidos pelo método play() da classe TapatanBoard
                             */
                            val playResult = treeRoot.board.play(treeRoot.player, movement, true)
                            when (playResult) {
                                PlayResult.SUCCESS -> {
                                    val nextNode = minimax.selectNodeWithMatchingMovement(treeRoot, movement)

                                    if (nextNode == null) {
                                        println(">>> Desculpe. A posição gerada não é esperada pelo jogo :(")
                                        println(">>> Pressione enter para encerrar...")
                                        readLine()
                                        System.exit(1)
                                    } else {
                                        shouldRequestMovement = false

                                        treeRoot = nextNode

                                        println(">>> Jogada aceita!")
                                    }
                                }

                                PlayResult.OCCUPIED_POSITION -> {
                                    println(">>> A posição de destino não está livre")
                                    println(">>> Pressione enter para tentar novamente...")
                                    readLine()
                                }

                                PlayResult.UNREACHABLE_POSITION -> {
                                    println(">>> As coordenadas informadas não estão dentro dos limites do tabuleiro.")
                                    println(">>> Use [A/a], [B/b], [C/c] para linha e 1..3 para coluna. Exemplo: \"A1,B1\"")
                                    println(">>> Pressione enter para tentar novamente...")
                                    println()
                                }

                                else -> {
                                    println(">>> Desculpe. O desenvolvedor do jogo é incompetente demais para deixar o jogo prosseguir :(")
                                    println(">>> Pressione enter para encerrar...")
                                    readLine()
                                    System.exit(1)
                                }
                            }
                        }
                    }
                }
            }

            printBoard()

            val alignmentCandidate = treeRoot.board.searchAlignment()

            if (alignmentCandidate != null) {
                println()
                summarizeResults(alignmentCandidate)
                println()

                print(">>> Deseja jogar novamente? [S/s] para sim: ")

                if ((readLine() ?: "").equals("s", true)) {
                    playCount = 0

                    treeRoot = minimax.buildTree(
                        TapatanBoard(_maxPlayer, _minPlayer),
                        MINIMAX_DEPTH_TREE
                    )
                } else {
                    keepGoing = false
                }
            }
        }
    }

    /**
     * Mapeia uma jogada manual para um PlayerMoviment. Por exemplo: se o input for A0,A1, então é retornado
     * um objeto PlayerMovement em que a coordenada de origem (source) é um par (0, 0), visto que A é a coluna
     * 0, e a coordenada de destino (destiny) é um par (0, 1)
     */
    private fun playerMovementFromInputString(input: String): PlayerMovement? =
        _regexPlayerMovement
            .matchEntire(input.trim())
            ?.groupValues
            ?.let { groups ->
                val stringToRow = { s: String ->
                    when (s.trim().toLowerCase()) {
                        "a" -> 0
                        "b" -> 1
                        "c" -> 2
                        else -> throw Throwable()
                    }
                }

                try {
                    PlayerMovement(
                        Pair(stringToRow(groups[1]), groups[2].toInt() - 1),
                        Pair(stringToRow(groups[3]), groups[4].toInt() - 1)
                    )
                } catch (_: Throwable) {
                    null
                }
            }

    /**
     * Sumariza os resultados acumulados por ambos os jogadores
     */
    private fun summarizeResults(alignment: BoardAlignment) {
        println("=".repeat(24))

        val alignmentDescription = when (alignment.type) {
            AlignmentType.VERTICAL -> "Vertical"
            AlignmentType.MAIN_DIAGONAL -> "Diagonal Principal"
            AlignmentType.SECONDARY_DIAGONAL -> "Diagonal Secundária"
            AlignmentType.HORIZONTAL -> "Horizontal"
        }
        val winningCoordinates = alignment.linedUpPieces.joinToString { piece ->
            val col = when (piece.position.first) {
                0 -> "A"
                1 -> "B"
                2 -> "C"
                else -> "?"
            }
            val row = piece.position.second + 1

            "$col$row"
        }

        println(">>> FIM DE JOGO!")
        println(">>> Vencedor: ${alignment.player.name}, com um alinhamento $alignmentDescription nas coordenadas $winningCoordinates")

        when (alignment.player.type) {
            PlayerType.MIN -> {
                _minPlayer.victories++
                _maxPlayer.losses++
            }

            else -> {
                _maxPlayer.victories++
                _minPlayer.losses++
            }
        }

        println(">>> Resultados:")
        println(">>> ${_minPlayer.name} ${_minPlayer.victories}  x  ${_maxPlayer.victories} ${_maxPlayer.name}")

        println("=".repeat(24))
    }

    companion object {
        /**
         * Profundidade máxima calculada a cada chamada ao Minimax
         */
        private const val MINIMAX_DEPTH_TREE = 8.toLong()
    }
}
