package ai.minimax

import core.Player
import core.PlayerMovement
import core.PlayerType
import tapatan.TapatanBoard
import java.util.*

/**
 * Representa um objeto apto a executar o algoritmo de Minimax
 */
class Minimax(private val startPlayer: Player, private val otherPlayer: Player) {

    /**
     * Profundidade calculada conforme mais níveis são demandados. Se buildTree for invocado, então este contador
     * é resetado
     */
    var depthReached = 0.toLong()
        private set(value) {
            if (!(value < 0 || value == field)) {
                field = value
            }
        }

    /**
     * Constrói a árvore de Minimax dados um jogador de início, o oponente deste, o tabuleiro montado sem quaisquer
     * jogadas e uma profundidade (se não fornecida, a árvore terá profundidade máxima). O retorno é uma referência
     * ao nó raiz dessa árvore, com todos os nodos já valorados
     */
    fun buildTree(kickoffBoard: TapatanBoard, depth: Long = Long.MAX_VALUE): TreeNode {
        val root = TreeNode(
            startPlayer,
            null,
            kickoffBoard,
            level = 1.toLong()
        )

        depthReached = 0

        demandMoreTreeLevels(root, depth)

        return root
    }

    /**
     * Demanda a geração de mais níveis da árvore Minimax, dado um nodo raiz
     */
    fun demandMoreTreeLevels(root: TreeNode, depth: Long) {
        val queueExpandableNodes = LinkedList<TreeNode>(listOf(root))
        val stopDepth = depthReached + depth

        while (queueExpandableNodes.isNotEmpty()) {
            val iteratorNode = queueExpandableNodes.remove()

            val currentPlayer = iteratorNode.player
            val nextPlayer = if (currentPlayer == startPlayer) otherPlayer else startPlayer
            val mapFeasibleBoards = generatePossibleBoards(iteratorNode.board, currentPlayer)

            for ((position, generators) in mapFeasibleBoards) {
                for ((newPosition, board) in generators) {
                    val child = TreeNode(
                        nextPlayer,
                        PlayerMovement(position, newPosition),
                        board,
                        level = iteratorNode.level + 1.toLong()
                    )

                    iteratorNode.children.add(child)

                    when (child.level) {
                        stopDepth -> depthReached = child.level
                        else -> if (board.searchAlignment() == null) {
                            queueExpandableNodes.add(child)
                        }
                    }
                }
            }
        }

        applyMinimax(root)
    }

    /**
     * Seleciona o nodo filho com o maior valor
     */
    fun selectMostValuableNode(root: TreeNode): TreeNode? = root.children.maxBy { it.value }

    /**
     * Seleciona o nodo filho tal que o movimento do jogador que o originou é igual ao fornecido como argumento
     */
    fun selectNodeWithMatchingMovement(root: TreeNode, movement: PlayerMovement): TreeNode? {
        val matches = root.children.filter { it.generatingMovement == movement }

        return if (matches.size == 1) {
            matches.first()
        } else {
            null
        }
    }

    /**
     * Gera os possíveis tabuleiros para o jogo, dados um tabuleiro qualquer e um jogador, o qual é referência
     * para determinar quais jogadas irão criar novos tabuleiros. Se o retorno desse método for vazio, então
     * mais nenhuma jogada é possível a partir da instância "generator". Em suma: cada posição possível (chave)
     * estará associada a uma coleção de tabuleiros acompanhados da posição que gerou esse tabuleiro diferente, i.e.,
     * o movimento efetivo do jogador
     */
    private fun generatePossibleBoards(
        generator: TapatanBoard,
        player: Player
    ): Map<Pair<Int, Int>, Iterable<Pair<Pair<Int, Int>, TapatanBoard>>> {

        val boards =
            mutableMapOf<Pair<Int, Int>, Iterable<Pair<Pair<Int, Int>, TapatanBoard>>>()

        for (row in generator.boardMatrix) {
            for (piece in row) {
                when (player) {
                    piece.owner -> {
                        val src = piece.position
                        val feasibleMovements =
                            generator
                                .calculateReachingMovements(src)
                                .filter { dst -> generator.isMovementValid(PlayerMovement(src, dst)) }


                        if (feasibleMovements.any()) {
                            boards[piece.position] =
                                feasibleMovements
                                    .map { newPosition ->
                                        val newBoard = generator.clone()
                                        newBoard.play(player, PlayerMovement(src, newPosition))

                                        Pair(newPosition, newBoard)
                                    }
                        }
                    }
                }
            }
        }

        return boards
    }

    /**
     * Calcula o valor de um nodo folha da árvore Minimax
     */
    private fun setValueForLeafNode(node: TreeNode) {
        val winner = node.board.searchAlignment()?.player?.type

        node.value = when (winner) {
            null -> 0.toDouble() /* Empate */
            else -> {
                val levelBaseValue = (depthReached - node.level + 1).toDouble()

                if (winner == PlayerType.MAX) {
                    levelBaseValue /* Vitória de MAX */
                } else {
                    -levelBaseValue /* Vitória de MIN */
                }
            }
        }
    }

    /**
     * Aplica a função de valor aos nodos da árvore Minimax. O algoritmo é em profundidade
     * e segue a regra básica: primeiro calcular o valor dos nodos folha e depois propagá-los,
     * conforme o jogador (MIN ou MAX), como o máximo ou mínimo dos valores obtidos pelos nodos filhos
     */
    private fun applyMinimax(node: TreeNode) {
        if (node.isLeaf()) {
            setValueForLeafNode(node)
        } else {
            for (c in node.children) {
                applyMinimax(c)
            }

            node.value = when (node.player.type) {
                PlayerType.MIN -> node.children.minBy { it.value }!!.value
                else -> node.children.maxBy { it.value }!!.value
            }
        }
    }
}
