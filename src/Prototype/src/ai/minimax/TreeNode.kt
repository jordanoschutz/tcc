package ai.minimax

import core.Player
import core.PlayerMovement
import tapatan.TapatanBoard

/**
 * Representa um nodo da árvore Minimax
 */
data class TreeNode(
    /**
     * O jogador que tem o nodo como possível jogada
     */
    val player: Player,
    /**
     * A posição que originou o nodo e acompanhado do movimento efetivo feito pelo jogador
     * para chegar nesse nodo. A origem é, portanto, a jogada a partir da qual se chega ao destino.
     * Note: nodos irmão têm generatingMovement.source iguais
     */
    val generatingMovement: PlayerMovement?,
    /**
     * Instância do tabuleiro que será verdadeira se o nodo
     * for escolhido
     */
    val board: TapatanBoard,
    /**
     * Resultado da aplicação da função de valor sobre o nodo. Para suporte a diferentes níveis de dificuldade,
     * este valor é real
     */
    var value: Double = 0.toDouble(),
    /**
     * Filhos do nodo, i.e., possíveis jogadas a partir dele
     */
    val children: MutableList<TreeNode> = mutableListOf(),
    /**
     * Indica em que nível da árvore está o nodo
     */
    val level: Long
) {
    /**
     * Indica se o nodo é folha
     */
    fun isLeaf() = children.isEmpty()

    /**
     * Indica se o nodo é raiz
     */
    fun isRoot() = generatingMovement == null
}
